import asyncio
import sys
import websockets

async def msg_handler(websocket):
    while True:
        password = input('password: ')
        if password == 'exit':
            # 退出程序
            sys.exit()
        # 发送需加密的密码
        await websocket.send(password)
        # 接收浏览器返回的加密后的密码
        encrypt_password = await websocket.recv()
        print('encrypt_password: ', encrypt_password)
        print('\n')
 
async def main_logic(websocket, path):
    await msg_handler(websocket)


start_server = websockets.serve(main_logic, '127.0.0.1', 5678)
asyncio.get_event_loop().run_until_complete(start_server)
# 一直运行，会阻塞程序
asyncio.get_event_loop().run_forever()