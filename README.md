## Play Selenium

## 前置知识

1.Python基础
2.Selenium基础

## 简介

通过Selenium实现浏览器自动化。

具体项目逻辑写到play目录下，每个项目里独立的项目名创建新的文件夹，以zapper项目为例，在play项目中，创建了zapper项目，然后再其中创建main.py写自动化该网站的逻辑，最后在根目录的main.py中使用该自动化逻辑。

你可以运行多次该程序，实现对多个Chrome的批量自动化操作，对于不同的部分，可以通过方法传参的方式解决。

## 环境搭建

搭建Selenium环境，参考[使用 Selenium 自动化你的浏览器](https://www.wangchucheng.com/zh/posts/selenium-basic-tutorial/)

安装Python环境依赖

```
pip install -r requirements.txt
```

## 使用

```
python main.py
```