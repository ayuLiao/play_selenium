from play.zapper.main import ZapperBrowser
from play.baidu.main import BaiDuBrowser
from logger import logger


def zapper_play():
    for i in range(5):
        try:
            result = ZapperBrowser().get_grecaptcha()
            break
        except Exception as e:
            logger.error(f' 第 {i} 次操作失败，重试中...,error: {e}', exc_info=True)
    print(' 结果: ', result)

def baidu_play():
    # BaiDuBrowser().search()
    BaiDuBrowser().save_pdf()


if __name__ == '__main__':
    # zapper_play()
    baidu_play()