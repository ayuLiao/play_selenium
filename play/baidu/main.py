import time
import json
from selenium.webdriver.chrome.options import Options

from base_browser import BaseBrowser
from logger import logger

class BaiDuBrowser(BaseBrowser):

    def __init__(self):
        self.options = Options()
        self.browser = self.get_browser()

    def search(self):
        url = 'https://www.baidu.com/'
        self.browser.get(url)
        print(1)

    def save_pdf(self):
        chrome_options = self.options
        settings = {
            "recentDestinations": [{
                    "id": "Save as PDF",
                    "origin": "local",
                    "account": "",
                }],
                "selectedDestinationId": "Save as PDF",
                "version": 2
            }
        prefs = {'printing.print_preview_sticky_settings.appState': json.dumps(settings)}
        chrome_options.add_experimental_option('prefs', prefs)
        chrome_options.add_argument('--kiosk-printing')
        brower = self.get_browser()
        brower.get("https://www.baidu.com/")
        brower.execute_script('window.print();')
        brower.quit()

# if __name__ == '__main__':
#     for i in range(5):
#         try:
#             result = ZapperBrowser().get_grecaptcha()
#             break
#         except Exception as e:
#             logger.error(f'第{i}次操作失败，重试中...')
#     print('结果: ', result)